var express = require('express');
var app = express();

var views = require('./config/views');
var locals = require('./config/locals');
var middlewares = require('./config/middlewares');
var routes = require('./config/routes');
var errors = require('./config/errors');

views.apply(app);
locals.apply(app);
middlewares.apply(app);
routes.apply(app);
errors.apply(app);

module.exports = app;
