var path = require('path');

// view engine setup
module.exports.apply = function (app) {
    app.set('views', path.join(__dirname, '..', 'views'));
    app.set('view engine', 'hbs');
}
