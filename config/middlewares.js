var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var express = require('express');

module.exports.apply = function (app) {
    // uncomment after placing your favicon in /public
    //app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
    app.use(logger('dev'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false, limit: 1024*1024 })); // we need about 1M
    app.use(cookieParser());
    app.use(require('node-sass-middleware')({
      src: path.join(__dirname, '..', 'public'),
      dest: path.join(__dirname, '..', 'public'),
      indentedSyntax: true,
      sourceMap: true
    }));
    app.use(express.static(path.join(__dirname, '..', 'public')));
}
