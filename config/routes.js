var index = require('../routes/index');
var create   = require('../routes/create');
var validate = require('../routes/validate');

module.exports.apply = function (app) {
    app.use('/', index);
    app.use('/validate', validate);
    app.use('/create', create);

    // catch 404 and forward to error handler
    app.use(function(req, res, next) {
      var err = new Error('Not Found');
      err.status = 404;
      next(err);
    });
}
