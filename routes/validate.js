import phantom from 'phantom';

var cssParser = require('css');
var express = require('express');
var router = express.Router();
var fetchUrl = require("fetch").fetchUrl;

const baseAttrReg = "\s*=\s*[\"\"']?([^'\"\" >]+?)[ '\"\"]";
const baseTagReg = baseAttrReg + "[^>]*?>";

const tagsToValidate = [
    {
        tagName: "a",
        tagPattern: "<a[^>]*?href" + baseTagReg,
        urlPattern: "href" + baseAttrReg
    },
    {
        tagName: "script",
        tagPattern: "<script[^>]*?src" + baseTagReg,
        urlPattern: "src" + baseAttrReg
    },
    {
        tagName: "link",
        tagPattern: "<link[^>]*?href" + baseTagReg,
        urlPattern: "href" + baseAttrReg
    },
    {
        tagName: "img",
        tagPattern: "<img[^>]*?src" + baseTagReg,
        urlPattern: "src" + baseAttrReg
    }
];

/* GET users listing. */
router.get('/', function (req, res, next) {
    var message;

    if (req.query.error) {
        message = 'An error has occurred, your Wrapper Url is invalid or empty';
    } else if (req.query.success) {
        message = 'Successfully uploaded!';
    }

    res.render('validate/index', {
        title: 'Doubledoors',
        wrapperUrl: 'http://www.thenewsguard.com/app/games/',
        message: message
    })
});

router.post('/validate-wrapper', function (req, res, next) {
    var wrapperUrl = req.body.wrapperUrl;

    if (!wrapperUrl) {
        res.redirect('/validate/?error=true');
        return;
    }

    var phantomTest = testPageInPhantom(wrapperUrl);

    getWrapperHtml(wrapperUrl)
        .then(function (wrapperHtml) {
            var htmlErrors = {};

            htmlErrors.invalidBaseTag = validateBaseTag(wrapperHtml);

            let resourcesList = getResourcesList(wrapperHtml);

            let cssAssetsTest = validateCssAssets(resourcesList);

            htmlErrors.invalidResources = validateResourcesUrls(resourcesList);

            htmlErrors.forbiddenMetaTags = validateMetaTags(wrapperHtml);

            Promise.all([phantomTest, cssAssetsTest])
                .then(function (data) {
                    var TestsResult = {
                        HtmlTest: htmlErrors,
                        PhantomTest: data[0],
                        CssTest: data[1]
                    };

                    res.render('validate/index', {
                        title: 'Doubledoors',
                        wrapperUrl: 'http://www.thenewsguard.com/app/games/',
                        testResult: TestsResult
                    });
                });
        });
});

/**
 * @param resourcesList
 * @returns {Promise.<Array[Object.stylesheetWarnings,parsingErrors]>}
 */
function validateCssAssets(resourcesList) {
    return fetchCssAssets(resourcesList)
        .then(function (fetchedStyles) {
            var allStylesErrors = [];

            fetchedStyles.forEach(function (fetchedCss) {
                if (fetchedCss.content.length === 0) {
                    return;
                }
                let result = parseStyles(fetchedCss.content);
                result.file = fetchedCss.file;
                allStylesErrors.push(result);
            });

            return allStylesErrors;
        })
        .catch(console.error.bind(console));
}

/**
 * @param resources
 * @returns {Promise.<Array[Object.file,content]>}
 */
function fetchCssAssets(resources) {
    let promises = [];

    resources.forEach(function (resourceUrl) {
        if (!isCssFile(resourceUrl)) {
            return;
        }
        let fetchPromise = new Promise(function (resolve, reject) {
            fetchUrl(resourceUrl, function (error, meta, body) {
                if (error) {
                    reject(error);
                }
                resolve({file: resourceUrl, content: body.toString()});
            });
        });
        promises.push(fetchPromise);
    });
    return Promise.all(promises);
}

/**
 * @param content
 * @returns Object.stylesheetWarnings,parsingErrors}
 */
function parseStyles(content) {
    let forbiddenSelectors = ['div', 'p', 'span', 'select', 'input', 'a', 'img'];

    let obj = cssParser.parse(content, {silent: true});
    let warnings = [];

    obj.stylesheet.rules.forEach(function (rule) {
        if (rule.type !== 'rule' || !rule.declarations || rule.declarations.length == 0) {
            return;
        }
        let warnSelectors = [];
        rule.selectors.forEach(function (selector) {
            if (forbiddenSelectors.indexOf(selector) !== -1) {
                warnSelectors.push(selector);
            }
        });
        if (warnSelectors.length !== 0) {
            warnings.push({
                selectors: warnSelectors.join(', '),
                start: rule.position.start,
                end: rule.position.end
            });
        }
    });
    return {stylesheetWarnings: warnings, parsingErrors: obj.stylesheet.parsingErrors};
}

/**
 * Load entire wrapper html
 * @param wrapperUrl
 * @returns {Promise}
 */
function getWrapperHtml(wrapperUrl) {
    return new Promise(function (resolve, reject) {
        fetchUrl(wrapperUrl, function (error, meta, body) {
            if (error) {
                reject();
            } else {
                resolve(body.toString());
            }
        });
    })
}

/**
 * @param wrapperHtml
 * @returns {Array}
 */
function validateMetaTags(wrapperHtml) {
    var metaRegex = new RegExp("<meta[^>]*?name\s*=\s*[\"\"']?(description|keywords)[ '\"\"][^>]*?>", 'g');
    var forbiddenMetaTags = [];
    var matchedElement;
    while ((matchedElement = metaRegex.exec(wrapperHtml)) !== null) {
        forbiddenMetaTags.push(matchedElement[0]);
    }
    return forbiddenMetaTags;
}

/**
 * @param string
 * @returns {void|XML|*}
 */
function unescape(string) {
    return string.replace(/\\"/g, '"');
}

/**
 * @param wrapperHtml
 * @returns {Array|{index: number, input: string}}
 */
function validateBaseTag(wrapperHtml) {
    var propertyRegExp = new RegExp("<base [^>]*>", 'g');
    return propertyRegExp.exec(wrapperHtml);
}

/**
 * Get all resources list
 * @param wrapperHtml
 * @returns {Array}
 */
function getResourcesList(wrapperHtml) {
    var resources = [];
    tagsToValidate.forEach(function (tag) {
        var elementRegExp = new RegExp(tag.tagPattern, 'g');
        var propertyRegExp = new RegExp(tag.urlPattern, 'g');

        var matchedElement;
        while ((matchedElement = elementRegExp.exec(wrapperHtml)) !== null) {

            var matchedProperty;
            while ((matchedProperty = propertyRegExp.exec(matchedElement)) !== null) {
                // Skip resources that have no asset extensions
                if ((/\.(js|css|ttf|svg|otf|woff2?|eot|gif|png|jpe?g|webp|ico)$/g).test(matchedProperty)) {
                    resources.push(matchedProperty[1]);
                }
            }
        }
    });
    return resources;
}

/**
 * Returns array of invalid resources
 * @param resources
 * @returns {Array}
 */
function validateResourcesUrls(resources) {
    var invalidUrls = [];
    resources.forEach(function (resourceUrl) {
        if (needUrlValidation(resourceUrl)) {
            if (isUrlAbsolute(resourceUrl)) {
                // TODO validate url for CORS
            } else {
                invalidUrls.push(resourceUrl);
            }

        }
    });
    return invalidUrls;
}

function validateResourceForCORS(resource) {
    return new Promise(function (resolve, reject, notify) {
        fetchUrl(resource, function (error, meta, body) {
            console.log('error', error);
            resolve('SUCCESS');
        });
    });
}

function isCssFile(url) {
    return /[^"]+\.css/.test(url);
}

function needUrlValidation(prop) {
    return !contains(prop, "//") && !contains(prop, "javascript:") && !contains(prop, "#") && !contains(prop, "mailto");
}

function contains(string, substring) {
    return string.indexOf(substring) !== -1;
}

function isUrlAbsolute(url) {
    return /^(?:\w+:)\/\//.test(url);
}

function testPageInPhantom(pageUrl) {
    var testResult = {
        requests: [],
        responses: [],
        errors: [],
        timeouts: [],
        arkContainer: {}
    };
    let sitepage;
    let phInstance;
    return new Promise(function (resolve, reject) {
        let phantomWaitTime = 3000; // in MS
        let phantomOptions = ['--web-security=true', '--ignore-ssl-errors=true', '--local-to-remote-url-access=true'];
        phantom.create(phantomOptions)
            .then(instance => {
                phInstance = instance;
                return instance.createPage();
            })
            .then(page => {
                sitepage = page;
                page.property('viewportSize', {width: 1024, height: 768});

                sitepage.on('onResourceRequested', function (requestData) {
                    //testResult.requests.push('Request (#' + requestData.id + '): ' + JSON.stringify(requestData));
                });
                sitepage.on('onResourceTimeout', function (request) {
                    testResult.timeouts.push('Response (#' + request.id + '): ' + JSON.stringify(request));
                });
                sitepage.on('onResourceReceived', function (response) {
                    //testResult.responses.push('Response (#' + response.id + ', stage "' + response.stage + '"): ' + JSON.stringify(response));
                });
                sitepage.on('onResourceError', function (resourceError) {
                    testResult.errors.push({
                        url: resourceError.url,
                        status: resourceError.status,
                        statusText: resourceError.statusText
                    });
                });
                sitepage.on('onError', function (msg, trace) {
                    testResult.errors.push('PhantomError::' + msg);
                });
                return page.open(pageUrl);
            })
            .then(status => {
                if (status !== 'success') {
                    console.log('PhantomJS::Unable to access page');
                } else {
                    console.log('PhantomJS::Page loaded');

                    return sitepage.evaluate(function () {
                        var element = document.getElementsByTagName("ark:gamebodycontainer")[0];
                        if (!element) {
                            return;
                        }
                        return element.parentElement.offsetWidth;
                    }).then(function (width) {
                        if (width == undefined) {
                            testResult.arkContainer.error = true;
                            testResult.arkContainer.message = 'ark:gamebodycontainer was not found'
                        } else {
                            if (width < 980) {
                                testResult.arkContainer.error = true;
                                testResult.arkContainer.message = 'ark:gamebodycontainer width is ' + width + 'px. Should be not less than 980px. Tested on 1024x768 viewport.'
                            } else {
                                testResult.arkContainer.error = false;
                            }
                        }

                        return new Promise(function (resolve) {
                            setTimeout(function () {
                                resolve(testResult.arkContainer);
                            }, phantomWaitTime);
                        })
                    });
                }
            })
            .then(arkContainer => {
                console.log('PhantomJS::Ok, exiting');
                sitepage.close();
                phInstance.exit();
                resolve(testResult);
            })
            .catch(error => {
                console.log("PhantomJS::Catch error");
                console.log(error);
                phInstance.exit();
                reject(error);
            });
    });
}

module.exports = router;
