var express = require('express');
var http = require('http');
var https = require('https');
var url = require('url');
var path = require('path');
var router = express.Router();

/* GET CREATE index. */
router.get('/', function(req, res, next) {
  res.render('create/index', { title: 'Wrapper tool' });
});

/* POST CREATE index. */
router.post('/', function(req, res, next) {
  res.render('create/index', { title: 'Wrapper tool', url: req.body.url });
});

/* POST CREATE download. */
router.post('/download', function(req, res, next) {
  res.attachment('wrapper.html');
  res.send(req.body.html);
});

/* GET proxy */
router.get('/*', function (req, res, next) {
  var rawHTML = '', myUrl = req.params[0];
  var urlObject = url.parse(myUrl),
    basedir = path.dirname(myUrl),
    host = urlObject.protocol  + '//' + urlObject.host,
    pathname = path.dirname(urlObject.pathname);

  downloadHtml(myUrl, function (html) {
    html.on('data', (data) => rawHTML += data);
    html.on('end', () => res.send(inject(absolute(rawHTML, host, basedir))));
  });
});

function downloadHtml(htmlURL, cb) {
  var urlObject = url.parse(htmlURL);
  if (urlObject.protocol === 'https:') {
    return https.get({
      host: urlObject.host,
      headers: {'user-agent': 'Mozilla/5.0'},
      path: urlObject.path
    }, cb);
  } else {
    return http.get(htmlURL, cb);
  }
}


function absolute(html, base, basedir) {
  return html.replace(/(src|href)="(.+?)"/g, function (match, attr, url) {
    if (isAbsolute(url)) return `${attr}="${url}"`;
    if (isRootRelative(url)) return `${attr}="${base}${url}"`;
    return `${attr}="${basedir}/${url}"`;
  });
}

function isAbsolute(val) {
  return val && val.match(/^(\w+:)*\/\//);
}

function isRootRelative(val) {
  return val && val.match(/^\//)
}

function inject(html) {
  return html.replace('</body>', '<script src="/javascripts/injector.js"></script></body>');
}

module.exports = router;
