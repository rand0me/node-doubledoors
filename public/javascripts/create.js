'use strict';

(function (w) {
  function Host(window) {
    this.window = window;
    this.activeBtn = null;
    this.client = null;
    this.buttons = this.window.document.querySelectorAll('[data-set-action]');
  }

  Host.prototype.send = function (msg) {
    this.client.postMessage(msg, '*');
  };

  Host.prototype.listen = function () {
    this.window.onmessage = function (msg) {
      if (msg.data === 'connect') this.client = msg.source;
    }.bind(this);
  };

  Host.prototype.bindHandlers = function () {
    this.buttons.forEach(function (btn) {
      btn.onclick = this[btn.getAttribute('data-set-action')].bind(this);
      btn.onmouseover = btn.onmouseout =  this.toggleDescription.bind(this);
    }.bind(this));
  };

  Host.prototype.setActive = function (btn) {
    if (this.activeBtn) this.activeBtn.classList.remove('active');
    btn.classList.add('active');
    this.activeBtn = btn;
  };

  Host.prototype.remove = function (e) {
    e.preventDefault();
    this.setActive(e.target);
    this.send({type: 'set', key: 'hover', value: 'highlight'});
    this.send({type: 'set', key: 'click', value: 'remove'});
  };

  Host.prototype.choose = function (e) {
    e.preventDefault();
    this.setActive(e.target);
    this.send({type: 'set', key: 'hover', value: 'highlight'});
    this.send({type: 'set', key: 'click', value: 'choose'});
  };

  Host.prototype.download = function (e) {
    e.preventDefault();
    this.setActive(e.target);
    this.send({type: 'execute', action: 'download'});
  };

  Host.prototype.reload = function (e) {
    e.preventDefault();
    this.setActive(e.target);
    this.send({type: 'execute', action: 'reload'});
  };

  Host.prototype.toggleDescription = function (e) {
    var btn = e.target;
    if (btn.description) {
      btn.description.parentNode.removeChild(btn.description);
      btn.description = null;
      return;
    }
    btn.description = this.window.document.createElement('div');
    btn.description.innerHTML = btn.dataset.description;
    btn.description.classList.add('wr-description');
    btn.description.style.width = '' + parseInt(btn.getBoundingClientRect().width - 28) + 'px';
    btn.description.style.left = '' + parseInt(btn.getBoundingClientRect().left) + 'px';
    btn.description.style.top = '' + parseInt(btn.getBoundingClientRect().bottom + 10) + 'px';
    this.window.document.body.appendChild(btn.description);
  };

  var host = new Host(w);
  host.bindHandlers();
  host.listen();
})(window);
