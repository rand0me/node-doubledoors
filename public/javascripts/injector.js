'use strict';

if (typeof window === 'undefined') {
  throw Error('This script runs only in browser.')
}

(function (w, d) {

  function Injector(window) {
    this.pickedElement = null;
    this.window = window;
    this.host = window.parent;
  }

  Injector.prototype.send = function (msg) {
    this.host.postMessage(msg, location.origin);
  };

  Injector.prototype.receive = function (msg) {
    if (msg.data.type === 'execute') {
      this[msg.data.action](msg.data);
    }
    if (msg.data.type === 'set') {
      this[msg.data.key] = this[msg.data.value];
      this.bindHandlers();
    }
  };

  Injector.prototype.connect = function () {
    this.send('connect');
    this.window.onmessage = this.receive.bind(this);
  };

  Injector.prototype.download = function () {
    var form = document.createElement('form');
    form.setAttribute('method', 'POST');
    form.setAttribute('action', '/create/download');
    var input = document.createElement('input');
    input.setAttribute('name', 'html');
    input.setAttribute('value', document.documentElement.innerHTML);
    form.appendChild(input);
    form.submit();
  };

  Injector.prototype.reload = function () {
    this.window.location.reload();
  };

  Injector.prototype.click = function (e) { e.preventDefault(); };

  Injector.prototype.hover = function (e) { e.preventDefault(); };

  Injector.prototype.bindHandlers = function () {
    this.window.document.body.onmousemove = this.hover.bind(this);
    this.window.document.body.onclick = this.click.bind(this);
  };

  Injector.prototype.highlight = function (e) {
    var el = this.window.document.elementFromPoint(e.clientX, e.clientY);
    if (this.prevel) this.prevel.style.boxShadow = '';
    el.style.boxShadow = '0 0 10px 2px hsla(120, 100%, 50%, .5)';
    this.prevel = el;
  };

  Injector.prototype.remove = function (e) {
    e.preventDefault();
    e.target.parentNode.remove(e.target);
  };

  Injector.prototype.choose = function (e) {
    this.addScript(e.target, 'http://games.mytoba.ca/manage/renderarena');
  };

  Injector.prototype.addScript = function (el, src) {
    var script = this.window.document.createElement('script');
    script.setAttribute('src', src);
    this.empty(el);
    el.classList.add('injector--place');
    el.appendChild(script);
  };

  Injector.prototype.empty = function (el) {
    while (el.firstChild) el.removeChild(el.firstChild)
  };

  var injector = new Injector(window);
  injector.connect();
  injector.bindHandlers();

})(window, document);
